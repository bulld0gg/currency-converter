<?php

namespace AppBundle\Utils;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Rate;

/**
 * The class is supposed to be used to import Euro foreign exchange reference rates
 * published by the European Central Bank in CSV format
 *
 * @link https://www.bank.lv/en/component/content/article/8692-euro-foreign-exchange-reference-rates-published-by-the-european-central-bank-in-csv-format
 */
class CsvRateLoader
{
    const CSV_URL = "https://www.bank.lv/vk/ecb.csv";

    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param DateTime $date
     *
     * @return bool
     */
    public function export(\DateTime $date)
    {
        $csvUrl = self::CSV_URL . '?date=' . $date->format('Ymd');
        $content = file_get_contents($csvUrl);

        if (0 === strlen($content) && $date->format('Ymd') == date("Ymd")) {
            // If no rates published for today fallback to yesterday rates
            $date->sub(new \DateInterval('P1D'));
            $csvUrl = self::CSV_URL . '?date=' . $date->format('Ymd');
            $content = file_get_contents($csvUrl);
        }

        $data = str_getcsv($content, "\n");

        foreach($data as $line) {

            $row = str_getcsv($line, "\t"); //parse the items in rows

            // First check if currency exists
            $rate = $this->entityManager->getRepository('AppBundle:Rate')->findOneBy(array('currency' => $row[0]));

            if (null !== $rate) {
                $rate->setRate($row[1]);
            } else {
                $rate = new Rate();
                $rate->setCurrency($row[0]);
                $rate->setRate($row[1]);
                $this->entityManager->persist($rate);
            }

            $this->entityManager->flush();
        }

        return true;
    }
}
