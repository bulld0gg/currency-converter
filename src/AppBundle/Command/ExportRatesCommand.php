<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Console command that is supposed to be used as a Cron job for importing currency rates
 * on daily basis
 *
 *     $ php app/console app:export-rates
 *
 */
class ExportRatesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:export-rates')
            ->setDescription('Export Euro foreign exchange reference rates published by the ECB in CSV format')
        ;
    }

    /**
     * Run CsvRateLoader service to export actual currency rates
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Starting Export: " . date("H:i:s"));

        $this->getContainer()->get('csv_rate_loader')->export(new \DateTime('now'));

        $output->writeln("Export Done: " . date("H:i:s"));
    }
}
