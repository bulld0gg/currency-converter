<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        //$this->get('csv_rate_loader')->export(new \DateTime());

        $rates = $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Rate')
            ->findBy(array(), array('currency' => 'ASC'));

        return $this->render('default/index.html.twig', array('rates' => $rates));
    }
}
