var Converter = {

    opts: {
        from_input: "base_input",
        from_select: "base_list",
        to_input: "target_input",
        to_select: "target_list",
        default_from: 'EUR',
        default_to: 'USD',
    },

    rates: {
        EUR:  1
    },

    init: function (rates, params) {

        for (var i = 0; i < rates.length; ++i) {
            this.rates[rates[i].currency] = parseFloat(rates[i].rate);
        }
        this.opts = $.extend(this.opts, params);

        this._fromInput = $("#" + this.opts.from_input).bind('keyup change', $.proxy(this.convertStraight, this));
        this._fromList = $("#" + this.opts.from_select).bind('change', $.proxy(this.convertStraight, this));
        this._toInput = $("#" + this.opts.to_input).bind('keyup change', $.proxy(this.convertBackwards, this));
        this._toList = $("#" + this.opts.to_select).bind('change', $.proxy(this.convertBackwards, this));

        this.populate(this._fromList);
        this.populate(this._toList);

        this._fromList.val(this.opts.default_from);
        this._toList.val(this.opts.default_to);

        this._fromInput.trigger('keyup');
    },

    populate: function(obj) {
        obj.find('option').remove();
        $.each(this.rates, function(currency, rate) {
            $('<option>').val(currency).text(currency).appendTo(obj);
        });
    },

    convertStraight: function(event) {

        // base -> EUR
        var value = parseFloat(this._fromInput.val()) / this.rates[this._fromList.val()];

        // EUR -> target
        value = Math.round(value * this.rates[this._toList.val()] * 100) / 100;

        return this._toInput.val(value);
    },

    convertBackwards: function(event) {

        // target -> EUR
        var value = parseFloat(this._toInput.val()) / this.rates[this._toList.val()];

        // EUR -> base
        value = Math.round(value * this.rates[this._fromList.val()] * 100) / 100;

        return this._fromInput.val(value);
    }

};

$(function() {
    Converter.init(rates);
});
